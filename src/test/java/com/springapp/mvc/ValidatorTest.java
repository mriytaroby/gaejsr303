package com.springapp.mvc;

import com.springapp.mvc.model.Item;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Author: Andriy Piskovyy <a.piskovyy@mriytaroby.com.ua>
 * Date: 11/7/13
 */
public class ValidatorTest {

    // Fields

    // Constructors

    // Overrided methods

    @Test
    public void testEntityValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Item item = new Item(123456, "qwerty");
        assertThat(validator.validate(item).size(), is(2));
        // TODO: check messages etc

        item = new Item(1245, "qwe");
        assertThat(validator.validate(item).size(), is(0));

        item = new Item(1245, "");
        assertThat(validator.validate(item).size(), is(1));

        item = new Item(1245, null);
        assertThat(validator.validate(item).size(), is(1));
    }

    // Methods

    // Getters & Setters

}
