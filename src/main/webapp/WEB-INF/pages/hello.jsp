<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/knockout/2.3.0/knockout-min.js'></script>
    <script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/knockout-validation/1.0.2/knockout.validation.min.js'></script>
</head>
<body>
	<h1>Test validation</h1>

    <form:form method="post" commandName="item" action="">
        <!-- Two-way binding. Populates textbox; syncs both ways. -->
        <p>Item ID:  <form:input path="id" data-bind="value: id" /></p>
        <p>Item Value: <form:input path="value" data-bind="value: value" /></p>

        <h3>Hello, <span data-bind="text: toString"> </span>!</h3>


        <form:button>Submit</form:button>
    </form:form>

    <div>
        <c:forEach items="${errors}" var="error">
            code = ${error.key}, message = ${error.value} <br/>
        </c:forEach>
    </div>

    <script type="text/javascript">

        ko.validation.rules.pattern.message = 'Invalid.';

        ko.validation.configure({
            registerExtenders: true,
            messagesOnModified: true,
            insertMessages: true,
            parseInputAttributes: true,
            messageTemplate: null
        });

        var ViewModel = function(id, value) {
            this.id = ko.observable(id).extend({  // custom message
                required: { message: 'Please supply your id.' },
                minLength: 1, maxLength: 3
            });
            this.value = ko.observable(value).extend({ minLength: 1, maxLength: 3 });

            this.toString = ko.computed(function() {
                return this.id() + " " + this.value();
            }, this);
        };

        ko.applyBindings(new ViewModel("${item.id}", "${item.value}"));
    </script>
</body>
</html>