package com.springapp.mvc.util;

import org.springframework.core.annotation.AnnotationUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Author: Andriy Piskovyy <a.piskovyy@mriytaroby.com.ua>
 * Date: 11/6/13
 */
public class RuleAssembler {

    public static Map<String, Map<String, Map<String, Object>>> getValidations(Class<?> target) throws IOException {
        Map<String, Map<String, Map<String, Object>>> validations = new HashMap<>();
        List<String> validationPackages = new ArrayList<>();

        // JSR 303
        validationPackages.add("javax.validation.constraints");
        // Hibernate
        validationPackages.add("org.hibernate.validator.constraints");

        Field[] fields = target.getDeclaredFields();
        for (Field field : fields) {
            boolean accessible = field.isAccessible();

            if (!accessible) {
                field.setAccessible(true);
            }
            Annotation[] annotations = field.getAnnotations();
            Map<String, Map<String, Object>> validationsMap = new HashMap<>();
            for (Annotation annotation : annotations) {
                if (validationPackages.contains(annotation.annotationType().getPackage().getName())) {
                    String annotationName = annotation.annotationType().getName();
                    Map<String, Object> annotationAtributes = AnnotationUtils.getAnnotationAttributes(annotation);
                    Iterator<String> it = annotationAtributes.keySet().iterator();
                    while (it.hasNext()) {
                        String key = it.next();
                        // Remove unneeded attributes
                        if ("payload".equals(key)) {
                            it.remove();
                        }
                    }
                    validationsMap.put(annotationName, annotationAtributes);
                }
            }
            Map<String, Object> typeMap = new HashMap<>();
            typeMap.put("type", field.getType());
            validationsMap.put("type", typeMap);
            if (validationsMap.size() > 0) {
                validations.put(field.getName(), validationsMap);
            }
        }
        return validations;
    }
}
