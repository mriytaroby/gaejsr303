package com.springapp.mvc.util;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.springapp.mvc.model.Item;

/**
 * Author: Andriy Piskovyy <a.piskovyy@mriytaroby.com.ua>
 * Date: 11/6/13
 */
public class OfyService {

    static {
        factory().register(Item.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}
