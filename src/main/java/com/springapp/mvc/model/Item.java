package com.springapp.mvc.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Author: Andriy Piskovyy <a.piskovyy@mriytaroby.com.ua>
 * Date: 11/6/13
 */
@Entity
public class Item {

    // Fields

    // see http://docs.jboss.org/hibernate/validator/4.0.1/reference/en/html_single/

    @NotNull(message="id is needed")
    @Digits(integer = 5, fraction = 0, message="id must be a 1-5 digit number")
    @Id
    Long id;

    @NotNull
    @Size(min=1, max=3, message="size is unsuitable")
    String value;

    // Constructors

    public Item() {
        super();
    }

    public Item(long l, String s) {
        this.id = l;
        this.value = s;
    }

    // Overrided methods

    // Methods

    // Getters & Setters


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
