package com.springapp.mvc;

import com.springapp.mvc.model.Item;
import com.springapp.mvc.util.RuleAssembler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.springapp.mvc.util.OfyService.ofy;

@Controller
@RequestMapping("/")
public class HelloController {

	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {

        Item item = ofy().load().type(Item.class).id(1L).now();

        if (item == null) {
            item = new Item(1L, "test value");
            ofy().save().entity(item).now();
        }

		model.addAttribute("item", item);

        return "hello";
	}

    @RequestMapping(value = "itemrules", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Map<String, Map<String, Object>>> getRules() {

        Map<String, Map<String, Map<String, Object>>> rules = null;
        try {
            rules = RuleAssembler.getValidations(Item.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rules;
    }

    @RequestMapping(method = RequestMethod.POST)
	public String saveOrUpdateItem(@ModelAttribute("item") @Valid Item item, BindingResult result, ModelMap model ) {

        if (result.hasErrors()) {

            Map<String, String> errors = new HashMap<>();

            for(ObjectError error: result.getAllErrors()) {
                String code = error.getCodes()[0];
                errors.put(code, error.getDefaultMessage());
            }
            model.addAttribute("errors", errors);
            return "hello";
        }

        ofy().save().entity(item).now();

        Item itemFromDB = ofy().load().type(Item.class).id(item.getId()).now();
		model.addAttribute("item", itemFromDB);

        return "hello";
	}


}